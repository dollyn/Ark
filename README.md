# Ark

#### 介绍
方舟 -- 可能是一个杞人忧天的项目

假如... 真的和美国闹僵了，脱钩了，完全没法来往了，海底光缆都断开了....
没有Google，StackOverflow等等这些网站，你还会写代码么...
所以这个项目是为了在这种情况万一不幸真的发生了，怎么办，能提前做哪些准备？



#### Google

似乎只能用Baidu代替了

#### StackOverflow
https://archive.org/details/stackexchange

 




#### 许可

署名-非商业性使用-相同方式共享 4.0 国际 (CC BY-NC-SA 4.0)
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh
https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.zh-Hans

